// ==UserScript==
// @name         ICC Shortcuts [LOCAL]
// @namespace    http://tampermonkey.net/
// @version      0.4
// @description  try to take over the world!
// @author       You
// @match        https://icc.edu/*
// @match        https://staging.icc.edu/*
// @match        https://iccdev.kinsta.cloud/*
// @require      file://C:\projects\user-scripts\icc-shortcuts-new.user.js
// @icon         https://www.google.com/s2/favicons?sz=64&domain=icc.edu
// @grant        none
// ==/UserScript==

(function($) {
    'use strict';

    console.log("User Script ICC Shortcuts...loaded from file");

    var styles = document.createElement('style');
    styles.innerText = /*css*/`
        .icc-shortcuts-modal ol > li::before {
            color: #58deff;
        }
        button#clear-history {
            color: #ff3434;
            cursor: pointer;
            font-weight: 500;
        }
        .icc-shortcuts-modal {
            width: 400px;
            min-height: 400px;
            max-height:80vh;
            top: 10;
            z-index: 100000000;
            position: fixed;
            top: 20px;
            left: 20px;
            background: #181818c9;
            padding: 1em;
            border-radius: 5px;
            overflow: auto;
            color: #EEE;
        }
        .icc-shortcuts-modal li{
            color: #EEE;
        }
        .icc-shortcuts-modal a,
        .icc-shortcuts-modal a:visited,
        .icc-shortcuts-modal a:focus {
            color: #FFF;
        }
        .icc-shortcuts-help {
            list-style:none;
            margin:0 0 1em;
            padding:0;
        }
        .icc-shortcuts-help strong {
            font-family:'Lucida Console';
            color:#3cfd5f;
        }
        #icc-shortcuts-search-result li span {
            font-size:10px;
            color:#bad155;
            margin-top:-8px;
            display:block;
        }
`.trim();
    document.querySelector('head').appendChild(styles);

    let pagePath = window.location.pathname;

    if (/^\/wp\-/.test(pagePath) || window.location.search.indexOf('preview_id') > -1) {
        console.log("TamperMonkey: ICC Shortcuts not running on admin or api pages")
        return;
    }

    let title = document.querySelector('title') || 'Unknown';
    title = title == 'Unknown' ? title : title.innerText.replace(' - Illinois Central College', '');

    if (title.toLowerCase().indexOf('page not found') > -1) {
        console.log("TamperMonkey: ICC Shortcuts not running on 404 page")
        return;
    }

    let shortcutList = [];
    const now = new Date();

    let config = localStorage.getItem('iccShortcutsConfig');
    config = config || '{}';
    config = JSON.parse(config);

    // Fetch histories from local storage

    let history = localStorage.getItem('history');
    history = history || '[]';
    history = JSON.parse(history);

    let historyRecent = localStorage.getItem('historyRecent');
    historyRecent = historyRecent || '[]';
    historyRecent = JSON.parse(historyRecent);

    // All-time history

    if (pagePath !== '/') {

        let prevHistory = history.find(elem => elem.path == pagePath);

        if (prevHistory) {
            prevHistory.title = title;
            prevHistory.visits += 1;
            prevHistory.ts = new Date();
        }
        else {
            history.push({
                title: title,
                path: pagePath,
                visits: 1,
                ts: new Date()
            });
        }

        history.sort((a, b) => {
            if (b.visits > a.visits) {
                return 1;
            }

            if (b.visits === a.visits) {
                return b.ts > a.ts;
            }

            return -1;
        });


        // Recent history

        let prevHistoryRecent = historyRecent.find(elem => elem.path == pagePath);

        if (prevHistoryRecent) {
            prevHistoryRecent.ts.push(now);
        }
        else {
            historyRecent.push({
                title: title,
                path: pagePath,
                ts: [now]
            });
        }
    }


    // Clean and save histories to local storage

    history.splice(100);
    localStorage.setItem('history', JSON.stringify(history));

    let currentDay = now.getDay();
    let numHours = currentDay < 3 ? 96 : 48
    let expireTime = numHours * 60 * 60 * 1000;
    let expireDate = now - expireTime;

    historyRecent.forEach(recent => {
        recent.ts = recent.ts.filter(ts => new Date(ts) > expireDate);
    });

    historyRecent = historyRecent.filter(recent => {
        return recent.ts.length > 0;
    });

    historyRecent.sort((a, b) => {
        return b.ts.length - a.ts.length;
    });

    localStorage.setItem('historyRecent', JSON.stringify(historyRecent));

    function openLink(gotoUrl, newTab = false) {
        if (newTab) {
            window.open(gotoUrl, '_blank');
            return;
        }

        window.location = gotoUrl;
    }


    function toggleEnvironment(dest = 'prod', newTab = false) {
        let host = window.location.hostname;
        let gotoUrl = `https://icc.edu${window.location.pathname}`;

        if (dest == 'prod' && host == 'icc.edu') {
            return;
        }
        else if (dest == 'staging') {
            if (host == 'staging.icc.edu') return;
          
            gotoUrl = `https://staging.icc.edu${window.location.pathname}`;
        }
        else if (dest == 'dev') {
            if (host == 'iccdev.kinsta.cloud') return;
            gotoUrl = `https://iccdev.kinsta.cloud${window.location.pathname}`;
        }

        openLink(gotoUrl, newTab);
    }

    function editPage(newTab = false) {
        let host = `https://${window.location.hostname}`;

        let gotoUrl = `${host}/wp-admin/post.php?post=${getPageId()}&action=edit`;
        //console.log(gotoUrl);
        openLink(gotoUrl, newTab);
    }

    function siteMedia(newTab = false) {
        let host = `https://${window.location.hostname}`;

        let gotoUrl = `${host}/wp-admin/upload.php`;
        openLink(gotoUrl, newTab);
    }

    function dashBoard(newTab = false) {
        let host = `https://${window.location.hostname}`;

        let gotoUrl = `${host}/wp-admin/`;
        openLink(gotoUrl, newTab);
    }

    function getPageId() {
        let bodyClass =  $('body').attr('class');
        let pageId = $('link[rel="shortlink"]').attr('href').split('?p=')[1];
        if (!pageId) pageId = bodyClass.match(/page\-id\-(\d+)/)[1];

        return pageId
    }

    function displayModal(html, id) {
        document.querySelectorAll('.icc-shortcuts-modal').forEach(elem => elem.remove());
        document.querySelector('body').insertAdjacentHTML('beforeend', `<div id="${id}" class="icc-shortcuts-modal">${html}</html>`);
    }

    function hideModal() {
        let modal = document.querySelector('.icc-shortcuts-modal')
        if (modal) {
            modal.remove();
            return modal.id;
        }

        return false;
    }

    function prettifyJSON(json) {
        let syntax = (JSON.stringify(json, null, 2));
        syntax = syntax
            .replace(/(\{|\}|\[|\])/g, '<span style="color:black">$1</span>')
            .replace(/(".+?"):/g, '<span style="color:blue">$1</span>:')
            .replace(/: (".+?")/g, ': <span style="color:green">$1</span>');

        return `<pre style="font-size:11px; background:#E6E6E6; line-height:16px"><code style="border:none; color:#7e7e7e;">${syntax}</code></pre>`;
    }

    function strong(str) {
        return `<strong style="font-family:Lucida Console; color:#3cfd5f;">${str}</strong>`;
    }

    async function graphQuery(query, variables = {}) {
        const response = await fetch('/graphql', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-REQUEST-TYPE': 'GraphQL',
                'Authorization': 'Basic ' + btoa(`${config.apiUsername}:${config.apiPassword}`)
            },
            body: JSON.stringify({
                query,
                variables
            })
        })

        const data = await response.json();

        return data;

    }

    function setApiConfig() {
        let username = prompt("Please Enter Api Username", "Api Username");
        if (username) config.apiUsername = username;

        let password = prompt("Please Enter Api Password", "Api Password");
        if (password) config.apiPassword = password;

        localStorage.setItem('iccShortcutsConfig', JSON.stringify(config));
    }

    document.onkeyup = async function(e) {
        if (e.key == "Escape") {
            hideModal();
            return;
        }
        if (e.ctrlKey && e.altKey && !e.shiftKey) {
            console.log(`ctrl+alt+${e.key}`)
            switch(e.key) {
                case 'ArrowRight':
                    toggleEnvironment('prod');
                    break;
                case 'ArrowDown':
                    toggleEnvironment('staging');
                    break;
                case 'ArrowLeft':
                    toggleEnvironment('dev');
                    break;
                case 'e': // e
                    // console.log(getPageBlogId());
                    editPage();
                    break;
                case 'i': {// i
                    let modalId = 'icc-shortcuts-info';
                    if (hideModal() === modalId) break;

                    let pageId = getPageId();
                    let html = `
<li>Page ID: <a style="color:#3cfd5f;" href="https://${window.location.hostname}/wp-json/wp/v2/pages/${pageId}">${pageId}</a></li>
`;
                    const query = `
query MyQuery ($id: ID!) {
  page(id: $id, idType: DATABASE_ID) {
    id
    title
    status
    template {
      templateName
    }
    modified
    date
    author {
      node {
        username
        firstName
        lastName
        email
      }
    }
    revisions(first: 1, where: {status: INHERIT}) {
      nodes {
        date
        status
        lastEditedBy {
          node {
            email
            nicename
            username
          }
        }
      }
    }
  }
}`;
                        const result = await graphQuery(query, { id: pageId });
                        const data = result.data;

                        const lastEditor = data.page.revisions.nodes.length > 0 ? data.page.revisions.nodes[0].lastEditedBy.node.username : data.page.author.username;

                        let datePub = new Date(data.page.date);
                        datePub = datePub.toLocaleDateString() + ' ' + datePub.toLocaleTimeString();

                        let dateMod = new Date(data.page.modified);
                        dateMod = dateMod.toLocaleDateString() + ' ' + dateMod.toLocaleTimeString();

                        var message = prettifyJSON(data);

                        html += `
<li>Last Editor: ${lastEditor}</li>
<li>Modified: ${dateMod}</li>
<li>Published: ${datePub}</li>
<li>Status: ${data.page.status}</li>
<li>Template: ${data.page.template.templateName}</li>
<li>Author: ${data.page.author.node.username}</li>
`;

                         html = `<ul style="padding-left:1em;">${html}</ul>`;

                        displayModal(`${html}<details><summary>Raw Data</summary>${message}</details>`, modalId);

                    break;
                }
                case 'm': // m
                    siteMedia();
                    break;
                case 'a': // a
                    var path = window.location.pathname;
                    var pathEncoded = path.replace(/\//g, '~2F');
                    openLink(`https://analytics.google.com/analytics/web/#/report/content-pages/a2017971w3588783p240149326/explorer-table.filter=${pathEncoded}&explorer-table.plotKeys=%5B%5D`, true);
                    break;
                case 'd': // d
                    dashBoard();
                    break;
                case 'h': {// h
                    let modalId = 'icc-shortcuts-history';
                    if (hideModal() === modalId) break;

                    let html = '';
                    let max = history.length > 9 ? 9 : history.length;
                    for (let i = 0; i < max; i++) {
                        const elem = history[i];
                        html += `<li><a href="${elem.path}">${elem.title}</a>&nbsp;(${elem.visits})</li>`;
                    }
                    if (history.length < 1) {
                        html += `<li>No history recorded</li>`;
                    }
                    html = `<ol style="padding-left:1em">${html}</ol><p><button id="clear-history">Clear</button><details><summary>Raw History Data</summary>${prettifyJSON(history)}</details>`;

                    displayModal(html, modalId);

                    shortcutList = history;

                    $('#clear-history').on('click', () => {
                        localStorage.removeItem('history');
                        history = [];
                        shortcutList = history;
                        hideModal();
                    });

                    break;
                }
                case 'f': {// f
                    let modalId = 'icc-shortcuts-search';
                    if (hideModal() === modalId) break;

                    if(!config.hasOwnProperty('apiUsername')) {
                        setApiConfig();
                    }

                    let html = `
<form id="icc-shortcuts-search-form">
<input type="text" name="query" placeholder="Find...">
<div id="icc-shortcuts-search-result"></div>
</form>
`;
                    displayModal(html, modalId);

                    $('#icc-shortcuts-search-form input').focus();
                    $('#icc-shortcuts-search-form').on('submit', async (e) => {
                        e.preventDefault();

                        let search = $('#icc-shortcuts-search-form input').val().trim();
                        const query = `
query MyQuery($search: String!) {
  pages(where: {search: $search}) {
    nodes {
      title(format: RAW)
      uri
    }
  }
}`;
                        const json = await graphQuery(query, { search });
                        const pages = json.data.pages.nodes;

                        let html = '';

                        let max = pages.length > 9 ? 9 : pages.length;
                        for (let i = 0; i < max; i++) {
                            const page = pages[i];
                            html += `<li><a href="${page.uri}">${page.title}</a><span>${page.uri}</span></li>`;
                        }
                        if (pages.length < 1) {
                            html += `<li>No results match your query</li>`;
                        }

                        html = `<ol style="padding-left:1em">${html}</ol>`;

                        $('#icc-shortcuts-search-result').html(html);

                        $('#icc-shortcuts-search-form input').focus().select();

                        shortcutList = pages.map(page => { return {path: page.uri} });
                    });

                    break;
                }
                case 'r': { // r
                    let modalId = 'icc-shortcuts-history-recent';
                    if (hideModal() === modalId) break;

                    let html = '';
                    let max = historyRecent.length > 9 ? 9 : historyRecent.length;
                    for (let i = 0; i < max; i++) {
                        const elem = historyRecent[i];
                        html += `<li><a href="${elem.path}">${elem.title}</a>&nbsp;(${elem.ts.length})</li>`;
                    }
                    if (history.length < 1) {
                        html += `<li>No history recorded</li>`;
                    }
                    html = `<ol style="padding-left:1em">${html}</ol><p><button id="clear-history">Clear</button><details><summary>Raw Recent Data</summary>${prettifyJSON(historyRecent)}</details>`;

                    displayModal(html, modalId);

                    shortcutList = historyRecent;

                    $('#clear-history').on('click', () => {
                        localStorage.removeItem('historyRecent');
                        historyRecent = [];
                        shortcutList = historyRecent;
                        hideModal();
                    });

                    break;
                }
                case '/': { // slash / ? - help
                    let modalId = 'icc-shortcuts-help';
                    if (hideModal() === modalId) break;

                    let html = `
<div style="color:#EEE">
<ul class="icc-shortcuts-help">
    <li>ctrl+alt+<strong>'i'</strong>: Page Info</li>
    <li>ctrl+alt+<strong>'e'</strong>: Edit*</li>
    <li>ctrl+alt+<strong>'d'</strong>: Dashboard*</li>
    <li>ctrl+alt+<strong>'m'</strong>: Media Library*</li>
    <li>ctrl+alt+<strong>'r'</strong>: Recent History</li>
    <li>ctrl+alt+<strong>'h'</strong>: History</li>
    <li>ctrl+alt+<strong>'f'</strong>: Find page</li>
    <li>ctrl+alt+<strong>'a'</strong>: Analytics</li>
    <li>ctrl+alt+➡️: View in Prod*</li>
    <li>ctrl+alt+⬇️: View in Staging*</li>
    <li>ctrl+alt+⬅️: View in Dev*</li>
</ul>
<p>* Open in new tab with ctrl+alt+shift</p>
<p>After a page search or opening a history screen:<br>ctrl+alt+#️⃣</p>
</div>
`;
                    displayModal(html, modalId);

                    break;
                }
                default: {
                    const key = e.key * 1;
                    if (key > 0 && key < 10) {

                        if (key > shortcutList.length) {
                            return;
                        }

                        let host = `https://${window.location.hostname}`;
                        let gotoUrl = `${host}${shortcutList[key-1].path}`;

                        openLink(gotoUrl, false);
                    }
                    break;
                }
            }
        }
        if (e.ctrlKey && e.altKey && e.shiftKey) {
            console.log(`ctrl+alt+shift+${e.key}`)
            switch(e.key) {
                case 'ArrowRight':
                    toggleEnvironment('prod', true);
                    break;
                case 'ArrowDown':
                    toggleEnvironment('staging', true);
                    break;
                case 'ArrowLeft':
                    toggleEnvironment('dev', true);
                    break;
                case 'e': // e
                    editPage(true);
                    break;
                case 'm': // m
                    siteMedia(true);
                    break;
                case 'd': // d
                    dashBoard(true);
                    break;
                case 'f': // f
                    setApiConfig();
                    break;
            }
        }
    };
})(jQuery);