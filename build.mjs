import shelljs from 'shelljs';

const args = process.argv;

// Clear dist folder
shelljs.rm('./dist/*.js');
// Copy files to dist folder
shelljs.cp('./*.js', './dist');

// Not implemented
let versionChange = false;
if (args.length === 3) {
    const action = args[2].split('=')[1];
    switch (action) {
        case 'minor':
            versionChange = 'minor';
            break;
            
        case 'major':
            versionChange = 'major';
            break;
        default:
            versionChange = action;
            break;
    }
}

// Figure out where repo is being pushed to
const remoteUrl = shelljs.exec('git remote -v',  {silent:true});
const gitPath = remoteUrl.substring(remoteUrl.indexOf(':') + 1, remoteUrl.indexOf('.git'));

// Update @require paths
shelljs.ls('./dist/*.js').forEach(file => {
    // https://gitlab.com/icc-web-public/user-scripts/-/raw/main/tdx-default.js

    const filename = file.substring(7);
    const remoteURL = `https://gitlab.com/${gitPath}/-/raw/main/dist/${filename}`;
    shelljs.sed('-i', `.*${filename}.*`, `// @updateURL    ${remoteURL}\n// @downloadURL  ${remoteURL}`, file);

    shelljs.sed('-i', 'file:\\/\\/C:\\\\projects\\\\user-scripts\\\\', `https://gitlab.com/${gitPath}/-/raw/main/dist/`, file);
    shelljs.sed('-i', '\/libs\\\\', '/libs/', file);
    shelljs.sed('-i', ' \\[LOCAL\\]', '', file);
    shelljs.sed('-i', '...loaded from file', '', file);
})