// ==UserScript==
// @name         TDX Tickets
// @namespace    http://tampermonkey.net/
// @version      0.11
// @description  try to take over the world!
// @author       You
// @match        https://hub.icc.edu/TDNext/Apps/565/Tickets/*
// @updateURL    https://gitlab.com/icc-web-public/user-scripts/-/raw/main/dist/tdx-tickets.user.js
// @downloadURL  https://gitlab.com/icc-web-public/user-scripts/-/raw/main/dist/tdx-tickets.user.js
// @icon         https://www.google.com/s2/favicons?sz=64&domain=icc.edu
// @grant        none
// ==/UserScript==

(function($) {
    'use strict';

    console.log('User Script TDX Tickets');

    if (window.location.href.indexOf('TicketDet') === -1 ) return;

    var styles = $.createElement('style');
    styles.innerText = /*css*/`
h2[id^="jump"],
#divFeed,
#divAttachments,
#divContent {
    content: '';
    display: block;
    margin-top: -160px;
    padding-top: 170px;
}
#divDetails a,
#divDescription,
#divCustomAttributes,
#divTags,
#icc-my-tasks a,
#icc-my-tasks button,
#icc-final-attachments a,
#icc-final-attachments button,
#divTasks a,
#divTasks button, 
#divAttachments a,
#divAttachments button {
    z-index: 1;
    position: relative;
}
.jump-list {
    font-size: 150%;
    margin: 0;
    padding: 2px 0 2px;
}
.jump-list strong {
    padding-left: 10px;
}
`;
    $.querySelector('head').appendChild(styles);

    // Listen for jQuery ajax events
    // When attachments ajax request loads, create final docs panel
    jQuery($).ajaxComplete(function ( event, request, settings ) { 
        if (settings.url.indexOf('Attachments') > -1) {
            let finalDocs = [];
            $.querySelectorAll('a[title="Download Attachment"]').forEach(elem => {
                const attachmentLabel = elem.innerText.toLowerCase();
                if (attachmentLabel.indexOf('final') > -1) {
                    finalDocs.push(elem);
                }
                if (attachmentLabel.match(/\.(jpe?g|png|webp|svg)$/)) {
                    const attachmentIcon = elem.closest('.media').querySelector('img');
                    attachmentIcon.width = 75;
                    attachmentIcon.src = elem.href;
                }
            })

            if (finalDocs.length) {
                var panel = $.createElement('div');
                panel.id = 'icc-final-attachments';
                panel.classList.add('panel', 'panel-success');
                panel.innerHTML = `
                    <div class="panel-heading">
                        <h2 class="h3 panel-title">Final Attachments</h2>
                    </div>
                `.trim();

                var panelBody = $.createElement('div');
                panelBody.classList.add('panel-body');

                finalDocs.forEach(elem => {
                    panelBody.innerHTML = panelBody.innerHTML + elem.closest('.media').outerHTML;
                })

                panel.appendChild(panelBody);
                let $myTasks = $.getElementById('icc-my-tasks');
                if ($myTasks) {
                    $myTasks.after(panel);
                }
                else {
                    $.querySelector('.panel-person-card').after(panel);
                }
            }
        }
    });

    // Remove onclick attribute
    // Disable opening in popup
    document.querySelectorAll('#divContent a[href^="/TDNext/Apps"]').forEach(elem => elem.removeAttribute('onclick'))

    // Make Task % Complete Visible
    document.querySelectorAll('#divTasks .sr-only').forEach(elem => elem.classList.remove('sr-only'))

    // Modifiy Details

    if (window.location.pathname.indexOf('TicketDet') > -1) {

        let jumpLinks = [
            { label: 'Top', anchor: 'divContent' },
        ];

        const addHeadingToDetails = function(triggerID, label, prependID = '') {
            const findAttr = $.getElementById(triggerID);
            if (findAttr) {
                const slug = label.replace(/( ?\/ ?|\s+)/g, '-').toLowerCase();
                var heading = $.createElement('h2');
                    heading.id = `jump-${slug}`;
                    heading.innerHTML = label;
                (prependID ? $.getElementById(prependID) : findAttr).prepend(heading);
                jumpLinks.push({label, anchor: `jump-${slug}`});
            }
        }

        // Creative
        addHeadingToDetails('ctlAttribute9342', 'Event', 'ctlAttribute9341');
        addHeadingToDetails('ctlAttribute9229', 'Creative');
        addHeadingToDetails('ctlAttribute9236', 'Email');
        addHeadingToDetails('ctlAttribute9257', 'Photography');
        addHeadingToDetails('ctlAttribute9260', 'PR / Media');
        addHeadingToDetails('ctlAttribute9264', 'Social Media');
        addHeadingToDetails('ctlAttribute9337', 'Web');
        addHeadingToDetails('ctlAttribute9268', 'Notes');

        jumpLinks.push(
            { label: 'Feed', anchor: 'divFeed' },
            { label: 'Files', anchor: 'divAttachments' }
        );
        // console.log('Jump:', jumpLinks);
        if (jumpLinks.length) {
            let jumpList = $.createElement('p');
            let jumpListHTML = `<strong>Jump to:</strong> `;
            const jumpNumLinks = jumpLinks.length;
            jumpLinks.forEach((link, i) => {
                jumpListHTML += `<a href="#${link.anchor}">${link.label}</a>`;
                if (i < jumpNumLinks -1) {
                    jumpListHTML += ' • ';
                }
            });
            jumpList.innerHTML = jumpListHTML;
            jumpList.className = 'jump-list';
            $.querySelector('#divTabHeader').append(jumpList);
        }
    }


    // Create my Tasks
    const createTasks = function(userID) {
        var panel = $.createElement('div');
            panel.id = 'icc-my-tasks';
            panel.classList.add('panel', 'panel-primary');
            panel.innerHTML = `
                <div class="panel-heading">
                    <h2 class="h3 panel-title">My Tasks</h2>
                </div>
            `.trim();
    
            var panelBody = $.createElement('div');
            var myTasks = $.querySelectorAll(`#upTasks a[href$="${userID}"]`);
    
            if (myTasks.length) {
                myTasks.forEach(elem => {
                    panelBody.innerHTML = panelBody.innerHTML + elem.closest('.padding').parentElement.outerHTML;
                });
    
                panel.appendChild(panelBody);
                $.querySelector('.panel-person-card').after(panel);
    
            }
    }

    // Get User ID and call create tasks
    const lsUserID = localStorage.getItem('tdx-user-id');
    if (lsUserID && lsUserID.length == 36) {
        createTasks(lsUserID);
    }
    else {
        window.onmessage = e => {
            if (e.data.hasOwnProperty('userID')) {
                createTasks(e.data.userID);
            }
        }
    }

    // Add button to ticket

    const ticketElem = $.querySelector('#litTicketName a');

    if (ticketElem) {
        let btnRow = $.getElementById('divButtons');
        btnRow.innerHTML += `
<a href="${ticketElem.getAttribute('href')}" class="btn btn-danger" type="button" title="To Ticket">
    <span class="fa fa-arrow-circle-o-left fa-nopad" aria-hidden="true"></span>
    <span class="hidden-xs padding-left-xs">To Ticket</span>
</a>
`;
    }

    // Add copy ticket link
    var ticketLink = $.createElement('button');
    ticketLink.classList.add('copyButton', 'pull-left');
    ticketLink.id = 'btnCopyLink';
    ticketLink.innerHTML = 'Copy URL';
    ticketLink.type = 'button';
    ticketLink.setAttribute('data-clipboard-text', window.location.href);
    const btnCopyID = $.getElementById('btnCopyID');
    if (btnCopyID) btnCopyID.after(ticketLink);

    // Sleep function
    const sleep = m => new Promise(r => setTimeout(r, m));

    // Get url from onclick attribute
    const parseOnclick = sel => {
        const elem = $.getElementById(sel);
        const onclick = elem.attributes.onclick.value;
        return onclick.split("'")[1];
    }

    document.onkeyup = async function(e) {
        if (e.key == "Escape") {
            return;
        }
        if (e.ctrlKey && e.altKey && !e.shiftKey) {
            console.log(`ctrl+alt+${e.key}`)
            switch(e.key) {
                case 'a':
                    window.location = parseOnclick('divReassignTicket');
                    break;
                case 'u':
                    window.location = parseOnclick('divUpdateFromActions');
                    break;
                case 'i':
                    $.querySelectorAll('label')[0].click();
                    break;
                case 'e':
                    window.location = parseOnclick('btnEdit');
                    break;
                case 'r':
                    $.getElementById('btnRefresh').click();
                    break;
                case 'f':
                    window.scrollTo(0, $.getElementById('divFeed').closest('.row').offsetTop - 150)
                    break;
                case 'l':
                   navigator.clipboard.writeText(window.location.href);
                   break;
            }
        }

    }
})(document);