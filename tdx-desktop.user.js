// ==UserScript==
// @name         TDX Desktop [LOCAL]
// @namespace    http://tampermonkey.net/
// @version      0.6
// @description  try to take over the world!
// @match        https://hub.icc.edu/TDNext/Home/Desktop/Desktop.aspx
// @match        https://hub.icc.edu/TDNext/Apps/565/Tickets/Desktop*
// @match        https://hub.icc.edu/TDNext/Apps/565/Reporting/ReportViewer*
// @match        https://hub.icc.edu/TDNext/Apps/565/Tickets/TicketSearch*
// @require      https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.4/moment.min.js
// @require      file://C:\projects\user-scripts\tdx-desktop.user.js
// @icon         https://www.google.com/s2/favicons?sz=64&domain=icc.edu
// @author       You
// @grant        none
// ==/UserScript==

(function($) {
    'use strict';

    console.log('User Script TDX Desktop...loaded from file');

    const targetNode = document.querySelectorAll(".ModuleContent");
    const config = { childList: true, subtree: true };

    var styles = $.createElement('style');
    styles.innerText = /*css*/`
.icc-status-indicator {
    width: 16px;
    height: 16px;
    border-radius:100%; 
    background:#CCC;
    display:inline-block;
    margin-right:6px
}
.icc-status--open { background: #FFF; border: solid 1px #CCC }
.icc-status--in-process { background: #6b92bf }
.icc-status--new { background: #6b92bf }
.icc-status--marketing-approval { background: #a54aa5 }
.icc-status--awaiting-client-response { background: #e3c000 }
.icc-status--project-strategy-meeting { background: #e79600 }
.icc-status--post-event-deliverables { background: #f5f51a }
.icc-status--writing-approval { background: #d30000 }
.icc-status--design-concept-approval { background: #c500c5 }
.icc-status--writing-deliverables { background: #2cd3d3 }
.icc-status--on-hold { background: #555 }
.icc-status--proofreading { background: #00c100 }
.icc-status--at-vendor { background: #CCAC93 }
.icc-status--closed, .icc-status--cancelled  { display: none }

.icc-web-label {
    border-radius:3px;
    background:#bbdbbb;
    padding: 2px 4px;
    margin: -2px -4px;
}

.icc-current-user {
    border-radius:3px;
    background:#caeff5;
    padding: 2px 4px;
    margin: -2px -4px;
}
`;
    $.querySelector('head').appendChild(styles);

    function slugify(str) {
        return str.replace(/[^a-z0-9]+(.)?/gi, (match, char) => (char ? '-' + char.toLowerCase() : '')).toLowerCase();
    }

    const observer = new MutationObserver((mutationList, observer) => {

        const userID = localStorage.getItem('tdx-user-id');
        
        document.querySelectorAll('table').forEach(elem => {
            if (elem.classList.contains('tamper')) {
                return;
            }

            // Remove onclick attribute
            // Disable opening in popup
            document.querySelectorAll('a[href*="/TDNext/Apps"]').forEach(elem => elem.removeAttribute('onclick'))

            // Add class to track if table has been processed or reset
            elem.classList.add('tamper');

            // Look for column headers and save position
            let modifyCols = [
                { label: 'Status', num: -1, relDate: true },
                { label: 'Created', num: -1, relDate: true },
                { label: 'Initial Resolved Date', num: -1, relDate: true },
                { label: 'Modified', num: -1, relDate: true },
                { label: 'Due', num: -1, relDate: true },
                { label: 'Start', num: -1, relDate: true },
                { label: 'Type', num: -1 },
                { label: 'Prim Resp', num: -1 }
            ];

            const relDateLabels = modifyCols.filter(i => i.relDate).map(i => i.label);

            elem.querySelectorAll('th, tr.TDGridHeader td').forEach((elem, i) => {
                modifyCols.forEach(col => {
                    if (elem.innerText.trim().indexOf(col.label) == 0) {
                        col.num = i + 1;
                    }
                })

            });

            // Loop through tagged columns
            modifyCols.forEach(col => {
                if ( col.num > 0 ) {

                    // Modify Status Column
                    if ( col.label == 'Status') {
                        elem.querySelectorAll(`tr td:nth-child(${col.num})`).forEach(elem => {
                            if (elem.innerText.trim() != '') {
                                elem.innerHTML = `<span class="icc-status-indicator icc-status--${slugify(elem.innerText.trim())}"></span> ${elem.innerText}`;
                            }
                        });
                    }

                    // Modify Type Column
                    if ( col.label == 'Type') {
                        elem.querySelectorAll(`tr td:nth-child(${col.num})`).forEach(elem => {

                            if ( elem.innerText.trim() == 'Website Request' ) {
                                elem.innerHTML = `<span class="icc-web-label">Website&nbsp;Request</span> `;
                                // elem.classList.add('icc-web-type')
                            }

                        });
                    }

                    // Modify Prim Resp Column
                    if ( col.label == 'Prim Resp') {
                        elem.querySelectorAll(`tr td:nth-child(${col.num}) a[href$="${userID}"]`).forEach(elem => {
                            // elem.innerHTML = `<span class="icc-status-indicator icc-status--${slugify(elem.innerText.trim())}"></span> ${elem.innerText}`;
                            elem.classList.add('icc-current-user');
                        });
                    }


                    // Modify Columns with Dates
                    if ( relDateLabels.indexOf(col.label) > -1 ) {
                        elem.querySelectorAll(`tr td:nth-child(${col.num})`).forEach((elem, i) => {

                            const dateText = elem.innerText.trim();

                            if (dateText) {
                                const parsedDate = new Date(dateText);
                                if (parsedDate == 'Invalid Date') return;

                                const relativeTime = moment.duration(moment(parsedDate.toISOString()) - moment());
                                if (relativeTime !== 'Invalid date') {
                                    elem.innerHTML = `<span title='${dateText}'>${relativeTime.humanize(true)}</span>`;
                                }

                                const relDays = relativeTime.asDays();

                                if (col.label == 'Due') {
                                    if (relativeTime.milliseconds() < 0) {
                                        elem.classList.add('red');
                                    }

                                    
                                    if (relativeTime.milliseconds() > 0 && relDays < 1.5) {
                                        elem.classList.add('green');
                                        elem.innerHTML = `<span class="glyphicon glyphicon-star" aria-hidden="true"></span> ${elem.innerHTML}`;
                                    }

                                    if (relDays >= 1.5 && relDays < 3) {
                                        elem.classList.add('blue');
                                        elem.innerHTML = `<span class="glyphicon glyphicon-flag" aria-hidden="true"></span> ${elem.innerHTML}`;
                                    }
                                }

                                if (col.label == 'Modified') {
                                    if (relDays > -1.5 && relDays < 0) {
                                        elem.classList.add('green');
                                        elem.innerHTML = `<span class="glyphicon glyphicon-star" aria-hidden="true"></span> ${elem.innerHTML}`;
                                    }

                                    if (relDays > -4 && relDays <= -1.5) {
                                        elem.classList.add('blue');
                                        elem.innerHTML = `<span class="glyphicon glyphicon-flag" aria-hidden="true"></span> ${elem.innerHTML}`;
                                    }
                                }
                            }
                        });
                    }
                }
            })
        });
    });
    observer.observe(document, config);
})(document);