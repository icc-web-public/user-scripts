// ==UserScript==
// @name         TDX Default [LOCAL]
// @namespace    http://tampermonkey.net/
// @version      0.3
// @description  try to take over the world!
// @match        https://hub.icc.edu/TDNext/Home/Desktop/Default.aspx
// @require      file://C:\projects\user-scripts\tdx-default.user.js
// @icon         https://www.google.com/s2/favicons?sz=64&domain=icc.edu
// @author       You
// @grant        none
// ==/UserScript==

(async function($) {
    'use strict';

    console.log('User Script TDX Default...loaded from file');

    $.getElementById('ui-id-1').addEventListener('click', e => {
        window.location.reload();
    });

    const portalLink = $.createElement('li');
    portalLink.className = 'ui-state-default ui-corner-top ui-tabs-tab ui-tab ui-sortable-handle';
    portalLink.innerHTML = `<a href="/TDClient/162/Portal/Home/?ID=195927d6-d629-4357-a105-a452f7d2adb6" class="ui-tabs-anchor">Employee Hub</a>`;
    const tabsList = $.getElementById('tabsList');

    tabsList.append(portalLink);

    const adminLink = portalLink.cloneNode(true);
    adminLink.innerHTML = `<a href="/TDAdmin/Home/Default.aspx?IsInTab=0" class="ui-tabs-anchor">Admin</a>`;
    tabsList.append(adminLink);

    const sandboxLink = portalLink.cloneNode(true);
    sandboxLink.innerHTML = `<a href="/SBTDClient/162/Portal/Home/" class="ui-tabs-anchor">Sandbox</a>`;
    tabsList.append(sandboxLink);



    const viewProfileOnclick = $.querySelector('.user-profile-menu a[onclick*="ViewMyProfile"]').getAttribute('onclick');
    const start = viewProfileOnclick.indexOf('?U=');
    const userID = viewProfileOnclick.substring(start + 3, start + 39);
    window.localStorage.setItem('tdx-user-id', userID);

    const appDesktop = $.getElementById('appDesktop');
    appDesktop.addEventListener('load', e => {
        appDesktop.contentWindow.postMessage({ userID: userID}, '*');
    });

})(document);