## Getting Started

1. On Chrome, install [Tampermonkey](https://chromewebstore.google.com/detail/tampermonkey/dhdgffkkebhmkfjojejmpbldmpobfkfo?hl=en) extension. For other browsers, Google *how to run userscripts*.
2. Go to the [dist](/dist) folder and location your desired script to install
3. Click on the file
4. Click on the *Open raw* button. It is in the middle of the button group right of the *Edit* button.
5. A new tab will open that looks blank and also be prompted by the Tampermonkey to install the script. 
6. Click *Install* and then close any tabs that are still open.

## Auto Updates in Tampermonkey

1. Locate the Tampermonkey icon in your extensions. Click on it and open the *Dashboard*
2. Click the *Settings* tab
3. Locate *Userscript Update* section and click on the drop down. Choose something other than *Never.* **Every day** is recommended setting.

## Install TDx Userscripts

1. Go to the [dist](/dist) folder
2. Locate all scripts that start with *tdx-*
3. Install each script following steps in *Getting Started*
